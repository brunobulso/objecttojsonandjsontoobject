//
//  BVBPessoa.m
//  TestePlist
//
//  Created by Bruno Vieira Bulso on 23/01/14.
//  Copyright (c) 2014 Bruno Vieira Bulso. All rights reserved.
//

#import "BVBPessoa.h"

@implementation BVBPessoa
- (instancetype) initWithNome : (NSString *) umNome
                    withIdade : (NSNumber *) umaIdade;
{
    self = [super init];
    
    if (self)
    {
        _nome = umNome;
       _idade = umaIdade;
    }
    return self;
}


- (NSMutableDictionary *)toNSDictionary
{
    
    NSMutableDictionary *dictionary = [[NSMutableDictionary alloc] init];
    [dictionary setValue:self.nome forKey:@"nome"];
    [dictionary setValue:self.idade forKey:@"idade"];
    
    return dictionary;
}

@end
