//
//  main.m
//  ObjectToJson
//
//  Created by Bruno Vieira Bulso on 24/01/14.
//  Copyright (c) 2014 Bruno Vieira Bulso. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "BVBAppDelegate.h"

int main(int argc, char * argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([BVBAppDelegate class]));
    }
}
