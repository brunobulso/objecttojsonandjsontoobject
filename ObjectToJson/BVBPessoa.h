//
//  BVBPessoa.h
//  TestePlist
//
//  Created by Bruno Vieira Bulso on 23/01/14.
//  Copyright (c) 2014 Bruno Vieira Bulso. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface BVBPessoa : NSObject
@property (nonatomic) NSString * nome;
@property (nonatomic) NSNumber * idade;
- (instancetype) initWithNome : (NSString *) umNome
                    withIdade : (NSNumber *) umaIdade;
- (NSMutableDictionary *)toNSDictionary;
@end
