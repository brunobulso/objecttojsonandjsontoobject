//
//  BVBViewController.m
//  TestePlist
//
//  Created by Bruno Vieira Bulso on 23/01/14.
//  Copyright (c) 2014 Bruno Vieira Bulso. All rights reserved.
//

#import "BVBViewController.h"
#import "BVBPessoa.h"


@interface BVBViewController ()
@property (nonatomic) UIImageView * imageView;
@property (nonatomic) NSMutableArray * listaPessoas;
@end

@implementation BVBViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    _imageView = [[UIImageView alloc] initWithFrame: CGRectMake(40, 50, 200, 260)];
    [self.view addSubview:_imageView];
    _listaPessoas = [[NSMutableArray alloc] init];
    [self createJson];	// Do any additional setup after loading the view, typically from a nib.
}


- (void) createJson
{
    //criando um objeto do tipo pessoa
    BVBPessoa * pessoa = [[BVBPessoa alloc] initWithNome:@"Bruno" withIdade: [[NSNumber alloc] initWithFloat:18.0]];
    //criando um array para armazenar as pessoas (caso tenha mais que uma)
    NSMutableArray * lista = [[NSMutableArray alloc] init];
    //adicionando o objecto criado na lista
    [lista addObject:[pessoa toNSDictionary]];
    
    //criando o erro que será apresentado se o Json não funcionar (não sei como podemos modificar de forma eficiente isso)
    NSError *error = nil;
    //criando o jsonData a partir da lista de Pessoas
    NSData *jsonDataOriundoDeUmObjetoPessoa = [NSJSONSerialization
                                               dataWithJSONObject:lista
                                               options:NSJSONWritingPrettyPrinted
                                               error:&error];
    //se o jsonData foi criado corretamente, iremos executar o if ali dentro
    if ([jsonDataOriundoDeUmObjetoPessoa length] > 0 &&
        error == nil){
        //NSLog com a data do objeto
        NSLog(@"Successfully serialized the dictionary into data = %@", jsonDataOriundoDeUmObjetoPessoa);
        //cria uma string com o conteudo do objeto Json
        NSString *jsonString = [[NSString alloc] initWithData:jsonDataOriundoDeUmObjetoPessoa
                                                     encoding:NSUTF8StringEncoding];
        //criando um objeto do tipo ID que recebe o objeto que está dentro do Json
        id objectFromJsonData = [NSJSONSerialization JSONObjectWithData:jsonDataOriundoDeUmObjetoPessoa options:0 error:&error];
        //verificando se o tipo do objeto é um Array
        if ([objectFromJsonData isKindOfClass: [NSArray class]])
        {
            //crio um array e inicializo com o conteudo do Json
            NSArray * arrayRetornoDoJson = [[NSArray alloc] initWithArray:objectFromJsonData];
            //preencho a listaPessoas com os dados contido no arrayRetornoDoJson
            for (int i = 0; i < arrayRetornoDoJson.count; i ++)
            {
                //o arrayRetornoJson possui em cada posicao um dicionario, com isso, criamos um novo objeto pessoa com as informacoes contidas no dicionario
                [_listaPessoas addObject: [[BVBPessoa alloc] initWithNome: [((NSDictionary *)arrayRetornoDoJson[i]) objectForKey:@"nome"]
                                                                withIdade:[((NSDictionary *)arrayRetornoDoJson[i]) objectForKey:@"idade"]]];
            }
            
            //percorro a listaPessoas
            for (int i = 0; i < _listaPessoas.count; i++)
            {
                //mostro na tela as informacoes (nome, idade) dos objetos da lista (que foram obtidos do JsonData)
                NSLog(@"Nome: %@ ----- Idade: %@", ((BVBPessoa *) _listaPessoas[i]).nome, ((BVBPessoa *) _listaPessoas[i]).idade);
            }
            
            NSLog(@"JSON String = %@", jsonString);
        }
        //se o não houve erro e o JsonData for igual a zero, então não armazenou nenhum dado
        else if ([jsonDataOriundoDeUmObjetoPessoa length] == 0 &&
                 error == nil){
            NSLog(@"No data was returned after serialization.");
        }
        //se der erro, é mostrado o erro
        else if (error != nil)
        {
            NSLog(@"An error happened = %@", error);
        }
    }
    
}
@end
